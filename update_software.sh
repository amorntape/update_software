#!/bin/bash

: '
	History update software
		[21/05/2561]	Initialize.
'

update_software(){
	### $1 = path project
	### $2 = path backup project
	### $3 = path software update
	### $4 = flag project.
	# echo $1, $2, $3, $4
	if [ $4 -eq 1 ]; then

		echo "###BACKUP DATA"
		cp -ar $1 $2
		wait

		echo "###UPDATE SOFTWARE"
		cp -ar $3 $1
		wait
	fi
}

restore_software(){
	### $1 = path project nortis
	### $2 = path backup project nortis
	### $3 = path project web config sbox
	### $4 = path backup project web config sbox
	# echo $1, $2, $3, $4
	check_file_sbox_bk=$(ls -1 $2/ | wc -l)
	# echo $check_file_sbox_bk
	if [ $check_file_sbox_bk -ge 1 ]; then

		# echo $check_file_sbox_bk
		rm -rf $1/*
		wait

		cp -ar $2/* $1
		wait
	fi

	check_file_web_sbox_bk=$(ls -1 $4/ | wc -l)
	# echo $check_file_web_sbox_bk
	if [ $check_file_web_sbox_bk -ge 1 ]; then

		# echo $check_file_web_sbox_bk
		rm -rf $3/*
		wait

		cp -ar $4/* $3
		wait
	fi
}

main(){

	### 0 = update software
	### 1 = restore software

	root_sbox="/home/nortis"
	bk_sbox="/home/back_up"

	root_web_sbox="/var/www/sbox"
	bk_web_sbox="/var/www/back_up"

	#Directory software
	dir_software="/home/nortis/update_software"
	if [ $1 -eq 0 ]; then
		
		###CHECK SOFTWARE
		echo "Checking for update software....."
		git_fetch=$(git fetch 2>&1)

		wait

		clgf=$(echo $git_fetch | grep "origin/master" | wc -l)
		if [ "$clgf" -le 0 ]; then
			echo "Your software is up to date."
			exit 0
		fi

		if [ ! -d "$dir_software" ]; then
			echo "Please check directory [$dir_software]"
			exit 0
		fi

		###VERIFY SOFTWARE
		echo "Verifying software....."

		echo "root path : $root_sbox"
		echo "root path : $root_web_sbox"

		if [ ! -d "$root_sbox" ]; then
			mkdir "$root_sbox"
			wait
			chgrp -R www-data "$root_sbox"
			wait
			chmod -R g+rwxs "$root_sbox"
			wait
			chmod 777 "$root_sbox"
			wait
			echo "Create path [$root_sbox] success."
		fi

		if [ ! -d "$root_web_sbox" ]; then
			mkdir "$root_web_sbox"
			wait
			chgrp -R www-data "$root_web_sbox"
			wait
			chmod -R g+rwxs "$root_web_sbox"
			wait
			chmod 777 "$root_web_sbox"
			wait
			echo "Create path [$root_web_sbox] success."
		fi

		if [ ! -d "$bk_sbox" ]; then
			mkdir "$bk_sbox"
			wait
			chgrp -R www-data "$bk_sbox"
			wait
			chmod -R g+rwxs "$bk_sbox"
			wait
			chmod 777 "$bk_sbox"
			wait
			echo "Create path [$bk_sbox] success."
		fi

		if [ ! -d "$bk_web_sbox" ]; then
			mkdir "$bk_web_sbox"
			wait
			chgrp -R www-data "$bk_web_sbox"
			wait
			chmod -R g+rwxs "$bk_web_sbox"
			wait
			chmod 777 "$bk_web_sbox"
			wait
			echo "Create path [$bk_web_sbox] success."
		fi

		wait

		git pull
		wait

		###UPDATE SOFTWARE
		echo "Starting update software....."
		
		program_update=$(ls -lrt $dir_software | grep "nortis" | wc -l)
		# echo $program_update
		update_software "$root_sbox/*" "$bk_sbox" "$dir_software/*" $program_update
		wait

		web_update=$(ls -lrt $dir_software | grep "sbox" | wc -l)
		# echo $web_update
		update_software "$root_web_sbox/*" "$bk_web_sbox" "$dir_software/*" $web_update
		wait

		echo "Update software success....."
	elif [ $1 -eq 1 ]; then
		echo "Restore software...."
		restore_software "$root_sbox" "$bk_sbox" "$root_web_sbox" "$bk_web_sbox"
		wait
	else
		echo "Please check input file update_software.sh."
	fi
}

if [ -z "$1" ]; then
    echo "****No argument supplied"
    exit 0
fi

main $1


